<?php

use Illuminate\Database\Seeder;
use App\Customfunctions\func;
class RolesTableSeeder extends Seeder
{
    use func;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $table = [
            [
                'role_id'   =>  1,
                'role_name' => 'admin',
                'role_description' => 'admin',
            ],
            [
                'role_id'   =>  2,
                'role_name' => 'student',
                'role_description' => 'student',
            ],
            [
                'role_id'   =>  3,
                'role_name' => 'coordinator',
                'role_description' => 'coordinator',
            ]

        ];
        DB::table('roles')->insert($table);
    }
}

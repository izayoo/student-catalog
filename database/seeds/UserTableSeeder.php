<?php

use Illuminate\Database\Seeder;
use App\Customfunctions\func;
class UserTableSeeder extends Seeder
{
    use func;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('users')->delete();
	    $table = [
		    [
			    'user_id' => 1,
			    'user_name' => 'administrator',
			    'email' => 'administr@tor.com',
			    'password' => bcrypt('password')
		    ],

	    ];
	    DB::table('users')->insert($table);
    }
}

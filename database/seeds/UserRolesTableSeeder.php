<?php

use Illuminate\Database\Seeder;
use App\Customfunctions\func;
class UserRolesTableSeeder extends Seeder
{
    use func;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('user_roles')->delete();
	    $table = [
		    [
			    'u_role_id' => 1,
			    'user_id' => 1,
			    'role_id' => 1
		    ],
	    ];
	    DB::table('user_roles')->insert($table);
    }
}

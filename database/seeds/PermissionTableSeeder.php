<?php

use Illuminate\Database\Seeder;
use App\Customfunctions\func;
class PermissionTableSeeder extends Seeder
{
    use func;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        $table = [
            [
                'permission_id'   =>  1,
                'permission_name' => 'users',
                'permission_description' => 'admin - users',
            ],
            [
                'permission_id'   =>  2,
                'permission_name' => 'students',
                'permission_description' => 'admin - students',
            ],
            [
                'permission_id'   =>  3,
                'permission_name' => 'logs',
                'permission_description' => 'admin - logs',
            ],
            [
                'permission_id'   =>  4,
                'permission_name' => 'profile',
                'permission_description' => 'public - profile',
            ],


        ];
        DB::table('permissions')->insert($table);
    }
}

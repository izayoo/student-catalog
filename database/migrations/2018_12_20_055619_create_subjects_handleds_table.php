<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsHandledsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects_handleds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employment_entry_id')->unsigned();
            $table->foreign('employment_entry_id')->references('employment_entry_id')->on('employments')->onDelete('cascade');
            $table->string('subject_name')->nullable();
            $table->string('subject_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects_handleds');
    }
}

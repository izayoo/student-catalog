<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
//            $table->increments('id');
	        $table->string('id')->primary();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->string('degree')->nullable();
            $table->string('specialization')->nullable();
            $table->string('family_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('birthplace')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('civil_status')->nullable();
            $table->string('present_occupation')->nullable();
            $table->string('position')->nullable();
            $table->string('employer_name')->nullable();
            $table->string('office_address')->nullable();
            $table->string('office_tel_no')->nullable();
            $table->integer('office_zip_code')->nullable();
            $table->string('office_fax_no')->nullable();
            $table->string('office_local_no')->nullable();
            $table->string('office_province')->nullable();
            $table->string('office_region')->nullable();
            $table->string('home_tel_no')->nullable();
            $table->integer('home_zip_code')->nullable();
            $table->string('home_fax_no')->nullable();
            $table->string('home_mobile_no')->nullable();
            $table->string('home_province')->nullable();
            $table->string('home_region')->nullable();
            $table->string('preferred_mailing_address')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('emer_cont_name')->nullable();
            $table->string('emer_cont_address')->nullable();
            $table->string('emer_cont_tel_no')->nullable();
            $table->string('ADNU_enrollee_before')->nullable();
            $table->string('financial_support')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}

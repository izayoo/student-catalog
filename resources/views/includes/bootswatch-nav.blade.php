<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Student Catalog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item @if(Request::segment(2) == 'users') active @endif">
                <a class="nav-link" href="/admin/users">Users
                    {{--<span class="sr-only">(current)</span>--}}
                </a>
            </li>
            <li class="nav-item @if(Request::segment(2) == 'students') active @endif">
                <a class="nav-link" href="/admin/students">Students
                    {{--<span class="sr-only">(current)</span>--}}
                </a>
            </li>
            <li class="nav-item @if(Request::segment(2) == 'logs') active @endif">
                <a class="nav-link" href="/admin/logs">Logs
                    {{--<span class="sr-only">(current)</span>--}}
                </a>
            </li>
        </ul>
        <a class="nav-item form-inline my-2 my-lg-0 btn" style="color: white" onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">Logout
        </a>
        <form id="logout-form" action="/logout" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</nav>
<!-- Side Navbar -->
<nav class="side-navbar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar"><img src="{{ url('') }}/admin/img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
        <div class="title">
            <h1 class="h4">Admin User</h1>
            <p>Administrator</p>
        </div>
    </div>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
        <li >
            <a href> <i class="icon-home"></i> Home</router-link> 
        </li>
        <li >
            <a href> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Orders</router-link> 
        </li>
    </ul>
</nav>
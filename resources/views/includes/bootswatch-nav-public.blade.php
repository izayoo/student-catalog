<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Student Catalog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        @if(Auth::check())
        <ul class="navbar-nav mr-auto">
            <li class="nav-item @if(Request::segment(1) == 'profile') active @endif">
                <a class="nav-link" href="/profile">Profile
                    {{--<span class="sr-only">(current)</span>--}}
                </a>
            </li>
        </ul>
        <a class="nav-item form-inline my-2 my-lg-0 btn" style="color: white" onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">Logout
        </a>
        <form id="logout-form" action="/logout" method="POST" style="display: none;">
            @csrf
        </form>
        @else
        <ul class="navbar-nav mr-auto"></ul>
        <a class="nav-item form-inline my-2 my-lg-0 btn" style="color: white" href="/login">Login
        </a>
        @endif
    </div>
</nav>
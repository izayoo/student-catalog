@extends('layouts.bootswatch')

@section('content')
    <div class="card-header">Login Logs</div>
    <div class="card-body">
        <table class="table">
            <thead class="table-primary">
            <td style="width: 25%">Username</td>
            <td style="width: 30%">Email</td>
            <td style="width: 20%">Role</td>
            <td style="width: 25%">Login Date</td>
            </thead>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->user->user_name}}</td>
                    <td>{{$user->user->email}}</td>
                    <td>{{$user->user->role->role->role_name}}</td>
                    <td>{{date('M d, Y H:i a', strtotime($user->login_at))}}</td>
                </tr>
            @endforeach
        </table>
        {{ $users->links() }}
    </div>
@endsection
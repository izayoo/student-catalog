@extends('layouts.bootswatch')

@section('content')
    <div class="card-header">Users</div>
    <div class="card-body">
        <div class="alert alert-warning" role="alert">
            All created users have a default password "<em>password</em>". Change password through "<em>forgot password</em>".
        </div>

        <div class="mb-3">
            <div class="col-md-9 form-inline" style="float: right;">
                <input type="text" id="searchField" class="form-control mr-2 col-md-9" placeholder="Search Username or Email" value="{{ $data['search'] or '' }}"/>
                <button id="search" class="btn btn-primary">Search</button>
            </div>
            <a href="/admin/users/create" class="btn btn-link">Add User</a>
        </div>
        <table class="table">
            <thead class="table-primary">
            <td style="width: 25%">Username</td>
            <td style="width: 30%">Email</td>
            <td style="width: 25%">Role</td>
            <td style="width: 20%">Operation</td>
            </thead>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->user_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->role_name}}</td>
                    <td>
                        <a class="btn btn-outline-primary" href="{{ url('/admin/users/' . $user->user_id) }}"><i class="fa fa-edit"></i> View</a>
                        @if(Auth::user()->user_id != $user->user_id)
                        <a class="btn btn-outline-danger btn" onclick="event.preventDefault();
                            document.getElementById('delete').submit();"><i class="fa fa-edit"></i> Delete</a>
                        <form id="delete" action="{{ url('/admin/users/' . $user->user_id) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE" >
                        </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $users->appends($data)->links() }}
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var search = function(){
                var searchField = $('#searchField').val();
                if(searchField != ''){
                    location.href = '/admin/users?search='+searchField;
                } else {
                    location.href = '/admin/users';
                }
            };

            $('#searchField').on('keypress', function(e){
                if(e.which == 13) {
                    search();
                }
            });

            $('#search').on('click', function(){
                search();
            });
        });
    </script>
@endsection
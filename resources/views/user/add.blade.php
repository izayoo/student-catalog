@extends('layouts.bootswatch')

@section('content')
    <div class="card-header">
        @if(isset($mode))
            View User
        @else
            Modify User
        @endif
    </div>
    <div class="card-body">
        @if(session('empty'))
            <div class="alert alert-danger" role="alert">
                {!! session('empty') !!}
            </div>
        @endif

        <form action="{{ url('/admin/users'.( isset($model) ? "/" . $model->user_id : "").(session('empty') ? '?redir=admin-students-create' : '')) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


            <div class="form-group">
                <label for="user_name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="user_name" id="user_name" class="form-control" value="{{$model['user_name'] or ''}}" required>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-6">
                    <input type="text" name="email" id="email" class="form-control" value="{{$model['email'] or ''}}" required>
                </div>
            </div>
            <div class="form-group">
                <label for="role" class="col-sm-3 control-label">Role</label>
                <div class="col-sm-6">
                    <select name="role" id="role" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{$role->role_id}}" {{isset($model['role']['role_id']) && $model['role']['role_id'] == $role->role_id ? 'selected' : '' }}>{{ucfirst($role->role_name)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    @if(isset($mode))
                        <a class="btn btn-outline-primary" href="{{ url('/admin/users'.( isset($model) ? "/" . $model->user_id : "").'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    @else
                        <button type="submit" class="btn btn-outline-success">
                            <i class="fa fa-plus"></i> Save
                        </button>
                    @endif
                    <a class="btn btn-outline-secondary" href="{{ url('/admin/users'.( isset($model) && !isset($mode) ? "/" . $model->user_id : "")) }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>
    </div>


@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            @if(isset($mode))
                $('body').find('input, select').attr('readonly', '').attr('disabled', '');
            @endif
        });
    </script>
@endsection
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    {{--<link href="{{ asset('css/components.css') }}" rel="stylesheet">--}}


    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('admin/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('admin/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('admin/css/fontastic.css') }}">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('admin/css/style.pink.css') }}" id="theme-stylesheet">

    <link href="{{ asset('css/admin-style.css') }}" rel="stylesheet">

    <script>
        var siteUrl = '{{url('')}}/';
    </script>

</head>
<body>
    @if(Auth::check())
        <div class="page">
        @include('includes.admin-nav')
        <div class="page-content d-flex align-items-stretch" id="app">
            @include('includes.admin-sidebar')
            <div class="content-inner" >
                @yield('content')
            </div>
        </div>
    @else
        <div class="page login-page">
        @yield('content')
    @endif
    @include('includes.admin-footer')
    @yield('scripts')
</div>



<!-- JavaScript files-->
<script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('admin/vendor/popper.js/umd/popper.min.js')}}"></script>
<script src="{{asset('admin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/vendor/jquery.cookie/jquery.cookie.js')}}"></script>
<script src="{{asset('admin/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('admin/vendor/jquery-validation/jquery.validate.min.js')}}"></script>

<!-- Main File-->
<script src="{{asset('admin/js/front.js')}}"></script>


</body>
</html>

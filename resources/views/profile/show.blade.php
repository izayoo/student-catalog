@extends('layouts.bootswatch-public')

@section('content')
    <div class="card-header">
        @if(isset($mode))
            View Profile
        @else
            Edit Profile
        @endif
    </div>
    <div class="card-body">
        <form action="{{ url('/profile/edit'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif

            @if(isset($users) && $users->count() != 0)
                <div class="form-group">
                    <label for="degree" class="col-sm-3 control-label">User</label>
                    <div class="col-sm-6">
                        <select name="user_id" id="user_id" class="form-control">
                            @foreach($users as $user)
                                <option value="{{$user->user_id}}">{{$user->user_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Student ID</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" value="{{$model['id'] or ''}}" readonly>
                </div>
            </div>

            <div class="form-group">
                <label for="degree" class="col-sm-3 control-label">Degree</label>
                <div class="col-sm-6">
                    <select name="degree" id="degree" class="form-control">
                        <option value="MSCS" {{isset($model['degree']) && $model['degree'] == 'MSCS' ? 'selected' : ''}}>MSCS</option>
                        <option value="MIT" {{isset($model['degree']) && $model['degree'] == 'MIT' ? 'selected' : ''}}>MIT</option>
                        <option value="MCGA" {{isset($model['degree']) && $model['degree'] == 'MCGA' ? 'selected' : ''}}>MCGA</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="specialization" class="col-sm-3 control-label">Specialization</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="specialization" id="specialization" class="form-control" value="{{$model['specialization'] or ''}}">--}}
                    <select name="specialization" id="specialization" class="form-control">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="family_name" class="col-sm-3 control-label">Family Name</label>
                <div class="col-sm-6">
                    <input type="text" name="family_name" id="family_name" class="form-control" value="{{$model['family_name'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="first_name" class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-6">
                    <input type="text" name="first_name" id="first_name" class="form-control" value="{{$model['first_name'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="middle_name" class="col-sm-3 control-label">Middle Name</label>
                <div class="col-sm-6">
                    <input type="text" name="middle_name" id="middle_name" class="form-control" value="{{$model['middle_name'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="gender" class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="gender" id="gender" class="form-control" value="{{$model['gender'] or ''}}">--}}
                    <select name="gender" id="gender" class="form-control">
                        <option value="Male" {{isset($model['gender']) && $model['gender'] == 'Male' ? 'selected' : ''}}>Male</option>
                        <option value="Female" {{isset($model['gender']) && $model['gender'] == 'Female' ? 'selected' : ''}}>Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="age" class="col-sm-3 control-label">Age</label>
                <div class="col-sm-2">
                    <input type="number" name="age" id="age" class="form-control" value="{{$model['age'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="date_of_birth" class="col-sm-3 control-label">Date Of Birth</label>
                <div class="col-sm-6">
                    <input type="date" name="date_of_birth" id="date_of_birth" class="form-control" value="{{$model['date_of_birth'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="birthplace" class="col-sm-3 control-label">Birthplace</label>
                <div class="col-sm-6">
                    <input type="text" name="birthplace" id="birthplace" class="form-control" value="{{$model['birthplace'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="citizenship" class="col-sm-3 control-label">Citizenship</label>
                <div class="col-sm-6">
                    <input type="text" name="citizenship" id="citizenship" class="form-control" value="{{$model['citizenship'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="civil_status" class="col-sm-3 control-label">Civil Status</label>
                <div class="col-sm-6">
                    {{--<input type="text" name="civil_status" id="civil_status" class="form-control" value="{{$model['civil_status'] or ''}}">--}}
                    <select name="civil_status" id="civil_status" class="form-control">
                        <option value="Single" {{isset($model['civil_status']) && $model['civil_status'] == 'Single' ? 'selected' : ''}}>Single</option>
                        <option value="Married" {{isset($model['civil_status']) && $model['civil_status'] == 'Married' ? 'selected' : ''}}>Married</option>
                        <option value="Widow/er" {{isset($model['civil_status']) && $model['civil_status'] == 'Widow/er' ? 'selected' : ''}}>Widow/er</option>
                        <option value="Separated" {{isset($model['civil_status']) && $model['civil_status'] == 'Separated' ? 'selected' : ''}}>Separated</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="present_occupation" class="col-sm-3 control-label">Present Occupation</label>
                <div class="col-sm-6">
                    <input type="text" name="present_occupation" id="present_occupation" class="form-control" value="{{$model['present_occupation'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="position" class="col-sm-3 control-label">Position</label>
                <div class="col-sm-6">
                    <input type="text" name="position" id="position" class="form-control" value="{{$model['position'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="employer_name" class="col-sm-3 control-label">Employer Name</label>
                <div class="col-sm-6">
                    <input type="text" name="employer_name" id="employer_name" class="form-control" value="{{$model['employer_name'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_address" class="col-sm-3 control-label">Office Address</label>
                <div class="col-sm-6">
                    <input type="text" name="office_address" id="office_address" class="form-control" value="{{$model['office_address'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_tel_no" class="col-sm-3 control-label">Office Tel No</label>
                <div class="col-sm-6">
                    <input type="text" name="office_tel_no" id="office_tel_no" class="form-control" value="{{$model['office_tel_no'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_zip_code" class="col-sm-3 control-label">Office Zip Code</label>
                <div class="col-sm-2">
                    <input type="number" name="office_zip_code" id="office_zip_code" class="form-control" value="{{$model['office_zip_code'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_fax_no" class="col-sm-3 control-label">Office Fax No</label>
                <div class="col-sm-6">
                    <input type="text" name="office_fax_no" id="office_fax_no" class="form-control" value="{{$model['office_fax_no'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_local_no" class="col-sm-3 control-label">Office Local No</label>
                <div class="col-sm-6">
                    <input type="text" name="office_local_no" id="office_local_no" class="form-control" value="{{$model['office_local_no'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_province" class="col-sm-3 control-label">Office Province</label>
                <div class="col-sm-6">
                    <input type="text" name="office_province" id="office_province" class="form-control" value="{{$model['office_province'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_region" class="col-sm-3 control-label">Office Region</label>
                <div class="col-sm-6">
                    <input type="text" name="office_region" id="office_region" class="form-control" value="{{$model['office_region'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="home_tel_no" class="col-sm-3 control-label">Home Tel No</label>
                <div class="col-sm-6">
                    <input type="text" name="home_tel_no" id="home_tel_no" class="form-control" value="{{$model['home_tel_no'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="home_zip_code" class="col-sm-3 control-label">Home Zip Code</label>
                <div class="col-sm-2">
                    <input type="number" name="home_zip_code" id="home_zip_code" class="form-control" value="{{$model['home_zip_code'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="home_fax_no" class="col-sm-3 control-label">Home Fax No</label>
                <div class="col-sm-6">
                    <input type="text" name="home_fax_no" id="home_fax_no" class="form-control" value="{{$model['home_fax_no'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="home_mobile_no" class="col-sm-3 control-label">Home Mobile No</label>
                <div class="col-sm-6">
                    <input type="text" name="home_mobile_no" id="home_mobile_no" class="form-control" value="{{$model['home_mobile_no'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="home_province" class="col-sm-3 control-label">Home Province</label>
                <div class="col-sm-6">
                    <input type="text" name="home_province" id="home_province" class="form-control" value="{{$model['home_province'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="home_region" class="col-sm-3 control-label">Home Region</label>
                <div class="col-sm-6">
                    <input type="text" name="home_region" id="home_region" class="form-control" value="{{$model['home_region'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="preferred_mailing_address" class="col-sm-3 control-label">Preferred Mailing Address</label>
                <div class="col-sm-6">
                    <select name="preferred_mailing_address" id="preferred_mailing_address" class="form-control">
                        <option value="Office" {{isset($model['preferred_mailing_address']) && $model['preferred_mailing_address'] == 'Office' ? 'selected' : ''}}>Office</option>
                        <option value="Home" {{isset($model['preferred_mailing_address']) && $model['preferred_mailing_address'] == 'Home' ? 'selected' : ''}}>Home</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-6">
                    <input type="text" name="email" id="email" class="form-control" value="{{$model['email'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="emer_cont_name" class="col-sm-3 control-label">Emergency Contact Name</label>
                <div class="col-sm-6">
                    <input type="text" name="emer_cont_name" id="emer_cont_name" class="form-control" value="{{$model['emer_cont_name'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="emer_cont_address" class="col-sm-3 control-label">Emergency Contact Adress</label>
                <div class="col-sm-6">
                    <input type="text" name="emer_cont_address" id="emer_cont_address" class="form-control" value="{{$model['emer_cont_address'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="emer_cont_tel_no" class="col-sm-3 control-label">Emergency Contact Tel No</label>
                <div class="col-sm-6">
                    <input type="text" name="emer_cont_tel_no" id="emer_cont_tel_no" class="form-control" value="{{$model['emer_cont_tel_no'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="ADNU_enrollee_before" class="col-sm-3 control-label">ADNU Enrollee Before</label>
                <div class="col-sm-6">
                    <select name="ADNU_enrollee_before" id="ADNU_enrollee_before" class="form-control">
                        <option value="Yes" {{isset($model['ADNU_enrollee_before']) && $model['ADNU_enrollee_before'] == 'Yes' ? 'selected' : ''}}>Yes</option>
                        <option value="No" {{isset($model['ADNU_enrollee_before']) && $model['ADNU_enrollee_before'] == 'No' ? 'selected' : ''}}>No</option>
                    </select>
                </div>
            </div>
            <div id="enrolmentDetail">
                <div class="form-group">
                    <label for="enrolment_type" class="col-sm-3 control-label">Enrollment Type</label>
                    <div class="col-sm-6">
                        <select name="enrolment_type" id="enrolment_type" class="form-control">
                            <option value="Regular" {{isset($model['adnuEnrolmentDetail']['enrolment_type']) && $model['adnuEnrolmentDetail']['enrolment_type'] == 'Regular' ? 'selected' : ''}}>Regular</option>
                            <option value="Non-degree" {{isset($model['adnuEnrolmentDetail']['enrolment_type']) && $model['adnuEnrolmentDetail']['enrolment_type'] == 'Non-degree' ? 'selected' : ''}}>Non-degree</option>
                            <option value="2-year" {{isset($model['adnuEnrolmentDetail']['enrolment_type']) && $model['adnuEnrolmentDetail']['enrolment_type'] == '2-year' ? 'selected' : ''}}>2-year</option>
                            <option value="Others" {{isset($model['adnuEnrolmentDetail']['enrolment_type']) && $model['adnuEnrolmentDetail']['enrolment_type'] == 'Others' ? 'selected' : ''}}>Others</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="previous_student_number" class="col-sm-3 control-label">Previous Student No.</label>
                    <div class="col-sm-6">
                        <input type="text" name="previous_student_number" id="previous_student_number" class="form-control" value="{{$model['adnuEnrolmentDetail']['previous_student_number'] or ''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="program" class="col-sm-3 control-label">Program</label>
                    <div class="col-sm-6">
                        <input type="text" name="program" id="program" class="form-control" value="{{$model['adnuEnrolmentDetail']['program'] or ''}}">
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="financial_support" class="col-sm-3 control-label">Financial Support</label>
                <div class="col-sm-6">
                    <input type="text" name="financial_support" id="financial_support" class="form-control" value="{{$model['financial_support'] or ''}}">
                </div>
            </div>
            <div class="form-group" id="loan">
                <label for="lender" class="col-sm-3 control-label">Loan Lender</label>
                <div class="col-sm-6">
                    <input type="text" name="lender" id="lender" class="form-control" value="{{$model['loanDetail']['lender'] or ''}}">
                </div>
            </div>
            <div id="scholarship">
                <div class="form-group">
                    <label for="type" class="col-sm-3 control-label">Scholarship Type</label>
                    <div class="col-sm-6">
                        <input type="text" name="type" id="type" class="form-control" value="{{$model['scholarshipDetail']['type'] or ''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="sponsor" class="col-sm-3 control-label">Scholarship Sponsor</label>
                    <div class="col-sm-6">
                        <input type="text" name="sponsor" id="sponsor" class="form-control" value="{{$model['scholarshipDetail']['sponsor'] or ''}}">
                    </div>
                </div>
            </div>
            <hr/>
            <div id="employment">
                <div class="row">
                    <h3 class="col-sm-6">Employment Details</h3>
                </div>
                <div class="form-group">
                    <label for="employment_name" class="col-sm-3 control-label">Employer Name</label>
                    <div class="col-sm-6">
                        <input type="text" name="employment_name" class="form-control" value="{{$model['employment']['employment_name'] or ''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="employment_address" class="col-sm-3 control-label">Employer Address</label>
                    <div class="col-sm-6">
                        <input type="text" name="employment_address" class="form-control" value="{{$model['employment']['employment_address'] or ''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="position" class="col-sm-3 control-label">Position</label>
                    <div class="col-sm-6">
                        <input type="text" name="position" class="form-control" value="{{$model['employment']['position'] or ''}}">
                    </div>
                </div>
            </div>
            <hr/>
            <div id="schoolAttended">
                <div class="row">
                    <h3 class="col-sm-6">School/s Attended</h3>
                </div>
                @if(!isset($mode)) <a id="addMoreSchoolAttended" class="btn btn-success">Add more</a> @endif
                <div class="addSchoolAttended hidden">
                    <a class="remove-parent btn btn-link">Remove</a>
                    <div class="form-group">
                        <label for="institution" class="col-sm-3 control-label">Institution</label>
                        <div class="col-sm-6">
                            <input type="text" name="schoolAttended[institution][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="degree" class="col-sm-3 control-label">Degree</label>
                        <div class="col-sm-6">
                            <input type="text" name="schoolAttended[degree][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="major_field" class="col-sm-3 control-label">Major Field</label>
                        <div class="col-sm-6">
                            <input type="text" name="schoolAttended[major_field][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="from" class="col-sm-3 control-label">From</label>
                        <div class="col-sm-6">
                            <input type="date" name="schoolAttended[from][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="to" class="col-sm-3 control-label">To</label>
                        <div class="col-sm-6">
                            <input type="date" name="schoolAttended[to][]" class="form-control">
                        </div>
                    </div>
                    <hr/>
                </div>
                @if(isset($model))
                    @foreach($model['schoolAttended'] as $sa)
                        <div class="addSchoolAttended">
                            @if(!isset($mode)) <a class="remove-parent btn btn-link">Remove</a> @endif
                            <div class="form-group">
                                <label for="institution" class="col-sm-3 control-label">Institution</label>
                                <div class="col-sm-6">
                                    <input type="text" name="schoolAttended[institution][]" class="form-control" value="{{$sa['institution'] or ''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="degree" class="col-sm-3 control-label">Degree</label>
                                <div class="col-sm-6">
                                    <input type="text" name="schoolAttended[degree][]" class="form-control" value="{{$sa['degree'] or ''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="major_field" class="col-sm-3 control-label">Major Field</label>
                                <div class="col-sm-6">
                                    <input type="text" name="schoolAttended[major_field][]" class="form-control" value="{{$sa['major_field'] or ''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="from" class="col-sm-3 control-label">From</label>
                                <div class="col-sm-6">
                                    <input type="date" name="schoolAttended[from][]" class="form-control" value="{{$sa['from'] or ''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="to" class="col-sm-3 control-label">To</label>
                                <div class="col-sm-6">
                                    <input type="date" name="schoolAttended[to][]" class="form-control" value="{{$sa['to'] or ''}}">
                                </div>
                            </div>
                            <hr/>
                        </div>
                    @endforeach
                @endif
            </div>
            {{--<hr/>--}}
            <div id="specialAward">
                <div class="row">
                    <h3 class="col-sm-6">Special Awards</h3>
                </div>
                @if(!isset($mode)) <a id="addMoreSpecialAward" class="btn btn-success">Add more</a> @endif
                <div class="addSpecialAward hidden">
                    <a class="remove-parent btn btn-link">Remove</a>
                    <div class="form-group">
                        <label for="special_award_type" class="col-sm-3 control-label">Type</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="specialAward[special_award_type][]">
                                <option value="Academic Honor">Academic Honor</option>
                                <option value="Award">Award</option>
                                <option value="Scholarship">Scholarship</option>
                            </select>'
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="special_award_name_title" class="col-sm-3 control-label">Name/Title</label>
                        <div class="col-sm-6">
                            <input type="text" name="specialAward[special_award_name_title][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="institution_conferring_award" class="col-sm-3 control-label">Institution Conferring Award</label>
                        <div class="col-sm-6">
                            <input type="text" name="specialAward[institution_conferring_award][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date_conferred" class="col-sm-3 control-label">Date Conferred</label>
                        <div class="col-sm-6">
                            <input type="date" name="specialAward[date_conferred][]" class="form-control">
                        </div>
                    </div>
                    <hr/>
                </div>
                @if(isset($model))                 @foreach($model['specialAward'] as $sa)
                    <div class="addSpecialAward">
                        @if(!isset($mode)) <a class="remove-parent btn btn-link">Remove</a> @endif
                        <div class="form-group">
                            <label for="special_award_type" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="specialAward[special_award_type][]">
                                    <option value="Academic Honor" {{$sa['special_award_type'] == 'Academic Honor' ? 'selected' : ''}}>Academic Honor</option>
                                    <option value="Award" {{$sa['special_award_type'] == 'Award' ? 'selected' : ''}}>Award</option>
                                    <option value="Scholarship" {{$sa['special_award_type'] == 'Scholarship' ? 'selected' : ''}}>Scholarship</option>
                                </select>'
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="special_award_name_title" class="col-sm-3 control-label">Name/Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="specialAward[special_award_name_title][]" class="form-control" value="{{$sa['special_award_name_title'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="institution_conferring_award" class="col-sm-3 control-label">Institution Conferring Award</label>
                            <div class="col-sm-6">
                                <input type="text" name="specialAward[institution_conferring_award][]" class="form-control" value="{{$sa['institution_conferring_award'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_conferred" class="col-sm-3 control-label">Date Conferred</label>
                            <div class="col-sm-6">
                                <input type="date" name="specialAward[date_conferred][]" class="form-control" value="{{$sa['date_conferred'] or ''}}">
                            </div>
                        </div>
                        <hr/>
                    </div>
                @endforeach                 @endif
            </div>
            <div id="publication">
                <div class="row">
                    <h3 class="col-sm-6">Publications</h3>
                </div>
                @if(!isset($mode)) <a id="addMorePublication" class="btn btn-success">Add more</a> @endif
                <div class="addPublication hidden">
                    <a class="remove-parent btn btn-link">Remove</a>
                    <div class="form-group">
                        <label for="special_award_name_title" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-6">
                            <input type="text" name="publication[publication_title][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date_published" class="col-sm-3 control-label">Date Published</label>
                        <div class="col-sm-6">
                            <input type="date" name="publication[date_published][]" class="form-control">
                        </div>
                    </div>
                    <hr/>
                </div>
                @if(isset($model))                 @foreach($model['publication'] as $pub)
                    <div class="addPublication">
                        @if(!isset($mode)) <a class="remove-parent btn btn-link">Remove</a> @endif
                        <div class="form-group">
                            <label for="special_award_name_title" class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="publication[publication_title][]" class="form-control" value="{{$pub['publication_title'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_published" class="col-sm-3 control-label">Date Published</label>
                            <div class="col-sm-6">
                                <input type="date" name="publication[date_published][]" class="form-control" value="{{$pub['date_published'] or ''}}">
                            </div>
                        </div>
                        <hr/>
                    </div>
                @endforeach                 @endif
            </div>
            <div id="affiliation">
                <div class="row">
                    <h3 class="col-sm-6">Affiliations</h3>
                </div>
                @if(!isset($mode)) <a id="addMoreAffiliation" class="btn btn-success">Add more</a> @endif
                <div class="addAffiliation hidden">
                    <a class="remove-parent btn btn-link">Remove</a>
                    <div class="form-group">
                        <label for="affiliation_type" class="col-sm-3 control-label">Type</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="affiliation[affiliation_type][]">
                                <option value="Professional">Professional</option>
                                <option value="Community">Community</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="affiliation_name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="affiliation[affiliation_name][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="affiliation_nature" class="col-sm-3 control-label">Nature</label>
                        <div class="col-sm-6">
                            <input type="text" name="affiliation[affiliation_nature][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="affiliation_position" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="affiliation[affiliation_position][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="from" class="col-sm-3 control-label">From</label>
                        <div class="col-sm-6">
                            <input type="date" name="affiliation[from][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="to" class="col-sm-3 control-label">To</label>
                        <div class="col-sm-6">
                            <input type="date" name="affiliation[to][]" class="form-control">
                        </div>
                    </div>
                    <hr/>
                </div>
                @if(isset($model))                 @foreach($model['affiliation'] as $af)
                    <div class="addAffiliation">
                        @if(!isset($mode)) <a class="remove-parent btn btn-link">Remove</a> @endif
                        <div class="form-group">
                            <label for="affiliation_type" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="affiliation[affiliation_type][]">
                                    <option value="Professional" {{$af['affiliation_type'] == 'Professional' ? 'selected' : ''}}>Professional</option>
                                    <option value="Community" {{$af['affiliation_type'] == 'Community' ? 'selected' : ''}}>Community</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="affiliation_name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="affiliation[affiliation_name][]" class="form-control" value="{{$af['affiliation_name'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="affiliation_nature" class="col-sm-3 control-label">Nature</label>
                            <div class="col-sm-6">
                                <input type="text" name="affiliation[affiliation_nature][]" class="form-control" value="{{$af['affiliation_nature'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="affiliation_position" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="affiliation[affiliation_position][]" class="form-control" value="{{$af['affiliation_position'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="from" class="col-sm-3 control-label">From</label>
                            <div class="col-sm-6">
                                <input type="date" name="affiliation[from][]" class="form-control" value="{{$af['from'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="to" class="col-sm-3 control-label">To</label>
                            <div class="col-sm-6">
                                <input type="date" name="affiliation[to][]" class="form-control" value="{{$af['to'] or ''}}">
                            </div>
                        </div>
                        <hr/>
                    </div>
                @endforeach                 @endif
            </div>
            <div id="thesis">
                <div class="row">
                    <h3 class="col-sm-6">Thesis</h3>
                </div>
                @if(!isset($mode)) <a id="addMoreThesis" class="btn btn-success">Add more</a> @endif
                <div class="addThesis hidden">
                    <a class="remove-parent btn btn-link">Remove</a>
                    <div class="form-group">
                        <label for="thesis_type" class="col-sm-3 control-label">Type</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="thesis[thesis_type][]">
                                <option value="Published">Published</option>
                                <option value="Unpublished">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="thesis_title" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-6">
                            <input type="text" name="thesis[thesis_title][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date_written" class="col-sm-3 control-label">Date Written</label>
                        <div class="col-sm-6">
                            <input type="date" name="thesis[date_written][]" class="form-control">
                        </div>
                    </div>
                    <hr/>
                </div>
                @if(isset($model))                 @foreach($model['thesis'] as $th)
                    <div class="addThesis">
                        @if(!isset($mode)) <a class="remove-parent btn btn-link">Remove</a> @endif
                        <div class="form-group">
                            <label for="thesis_type" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="thesis[thesis_type][]">
                                    <option value="Published" {{$th['thesis_type'] == 'Published' ? 'selected' : ''}}>Published</option>
                                    <option value="Unpublished" {{$th['thesis_type'] == 'Unpublished' ? 'selected' : ''}}>Unpublished</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="thesis_title" class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="thesis[thesis_title][]" class="form-control" value="{{$th['thesis_title'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_written" class="col-sm-3 control-label">Date Written</label>
                            <div class="col-sm-6">
                                <input type="date" name="thesis[date_written][]" class="form-control" value="{{$th['date_written'] or ''}}">
                            </div>
                        </div>
                        <hr/>
                    </div>
                @endforeach                 @endif
            </div>

            <div id="disability">
                <div class="row">
                    <h3 class="col-sm-6">Disabilities</h3>
                </div>
                @if(!isset($mode)) <a id="addMoreDisability" class="btn btn-success">Add more</a> @endif
                <div class="addDisability hidden">
                    <a class="remove-parent btn btn-link">Remove</a>
                    <div class="form-group">
                        <label for="disability_type" class="col-sm-3 control-label">Type</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="disability[disability_type][]">
                                <option value="Physical">Physical</option>
                                <option value="Learning">Learning</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="disability_name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="disability[disability_name][]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="disability_description" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-6">
                            <input type="text" name="disability[disability_description][]" class="form-control">
                        </div>
                    </div>
                    <hr/>
                </div>
                @if(isset($model))                 @foreach($model['disability'] as $dis)
                    <div class="addDisability">
                        @if(!isset($mode)) <a class="remove-parent btn btn-link">Remove</a> @endif
                        <div class="form-group">
                            <label for="disability_type" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="disability[disability_type][]">
                                    <option value="Physical" {{$dis['disability_type'] == 'Physical' ? 'selected' : ''}}>Physical</option>
                                    <option value="Learning" {{$dis['disability_type'] == 'Learning' ? 'selected' : ''}}>Learning</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="disability_name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="disability[disability_name][]" class="form-control" value="{{$dis['disability_name'] or ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="disability_description" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-6">
                                <input type="text" name="disability[disability_description][]" class="form-control" value="{{$dis['disability_description'] or ''}}">
                            </div>
                        </div>
                        <hr/>
                    </div>
                @endforeach                 @endif
            </div>

            <br/>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    @if(isset($mode))
                        <a class="btn btn btn-outline-primary" href="{{ url('/profile/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    @else
                        <button type="submit" class="btn btn-outline-success">
                            <i class="fa fa-plus"></i> Save
                        </button>
                        <a class="btn btn-outline-secondary" href="{{ url('/profile') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                    @endif
                </div>
            </div>

        </form>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            if($('#degree').val() == 'MSCS')
                $('#specialization').html('<option value="Applied Computing" {{isset($model['specialization']) && $model['specialization'] == 'Applied Computing' ? 'selected' : ''}}>Applied Computing</option><option value="Graphics and Computer Vision" {{isset($model['specialization']) && $model['specialization'] == 'Graphics and Computer Vision' ? 'selected' : ''}}>Graphics and Computer Vision</option>');
            else if($('#degree').val() == 'MIT')
                $('#specialization').html('<option value="System Development" {{isset($model['specialization']) && $model['specialization'] == 'System Development' ? 'selected' : ''}}>System Development</option><option value="Information Systems" {{isset($model['specialization']) && $model['specialization'] == 'Information Systems' ? 'selected' : ''}}>Information Systems</option>');
            else
                $('#specialization').html('<option value="General" {{isset($model['specialization']) && $model['specialization'] == 'General' ? 'selected' : ''}}>General</option><option value="Scientific Computing Game Design, Development and Production" {{isset($model['specialization']) && $model['specialization'] == 'Scientific Computing Game Design, Development and Production' ? 'selected' : ''}}>Scientific Computing Game Design, Development and Production</option><option value="Digital Arts, Animation and Production" {{isset($model['specialization']) && $model['specialization'] == 'Digital Arts, Animation and Production' ? 'selected' : ''}}>Digital Arts, Animation and Production</option>');

            if($('#ADNU_enrollee_before').val() == 'Yes') $('#enrolmentDetail').css('display', 'block');
            else $('#enrolmentDetail').css('display', 'none');

            if($('#financial_support').val().toLowerCase() == 'loan') {
                $('#loan').css('display', 'block');
                $('#scholarship').css('display', 'none');
            }
            else if($('#financial_support').val().toLowerCase() == 'scholarship'){
                $('#loan').css('display', 'none');
                $('#scholarship').css('display', 'block');
            }
            else{
                $('#loan').css('display', 'none');
                $('#scholarship').css('display', 'none');
            }

            @if(isset($mode))
            $('body').find('input, select').attr('readonly', '').attr('disabled', '');
            @else
                $('#degree').on('change', function () {
                        if($(this).val() == 'MSCS')
                            $('#specialization').html('<option value="Applied Computing">Applied Computing</option><option value="Graphics and Computer Vision">Graphics and Computer Vision</option>');
                        else if($(this).val() == 'MIT')
                            $('#specialization').html('<option value="System Development">System Development</option><option value="Information Systems">Information Systems</option>');
                        else
                            $('#specialization').html('<option value="General">General</option><option value="Scientific Computing Game Design, Development and Production">Scientific Computing Game Design, Development and Production</option><option value="Digital Arts, Animation and Production">Digital Arts, Animation and Production</option>');
                    });

            $('#ADNU_enrollee_before').on('change', function () {
                if($(this).val() == 'Yes') $('#enrolmentDetail').css('display', 'block');
                else $('#enrolmentDetail').css('display', 'none');
            });

            $('#financial_support').on('input', function () {
                if ($(this).val().toLowerCase() == 'loan') {
                    $('#loan').css('display', 'block');
                    $('#scholarship').css('display', 'none');
                }
                else if ($(this).val().toLowerCase() == 'scholarship') {
                    $('#loan').css('display', 'none');
                    $('#scholarship').css('display', 'block');
                }
                else {
                    $('#loan').css('display', 'none');
                    $('#scholarship').css('display', 'none');
                }
            });

            $('#addMoreSchoolAttended').on('click', function(){
                $(".addSchoolAttended").first().clone().removeClass('hidden').appendTo("#schoolAttended");
            });
            $('#addMoreDisability').on('click', function(){
                $(".addDisability").first().clone().removeClass('hidden').appendTo("#disability");
            });
            $('#addMoreSpecialAward').on('click', function(){
                $(".addSpecialAward").first().clone().removeClass('hidden').appendTo("#specialAward");
            });
            $('#addMorePublication').on('click', function(){
                $(".addPublication").first().clone().removeClass('hidden').appendTo("#publication");
            });
            $('#addMoreAffiliation').on('click', function(){
                $(".addAffiliation").first().clone().removeClass('hidden').appendTo("#affiliation");
            });
            $('#addMoreThesis').on('click', function(){
                $(".addThesis").first().clone().removeClass('hidden').appendTo("#thesis");
            });

            $('body').delegate('.remove-parent','click', function(){
                $(this).parent().remove();
            });
            @endif
        });
    </script>
@endsection
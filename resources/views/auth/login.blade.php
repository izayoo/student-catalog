
@extends('layouts.admin-master')
@section('content')

        <div class="container d-flex align-items-center">
          <div class="form-holder has-shadow">
            <div class="row">
              <!-- Logo & Information Panel-->
              <div class="col-lg-6">
                <div class="info d-flex align-items-center">
                  <div class="content">
                    <div class="logo">
                      <h1>Online Student Data Sheet Manager</h1>
                    </div>
                    <p>ADNU CCS Graduate Programs</p>
                  </div>
                </div>
              </div>
              <!-- Form Panel    -->
              <div class="col-lg-6 bg-white">
                <div class="form d-flex align-items-center">
                  <div class="content">
                    <form method="post" class="form-validate" action="/login">
                        {{ csrf_field() }}
                      <div class="form-group">
                        <input id="login-username" type="email" name="email" required data-msg="Please enter your email" class="input-material">
                        <label for="login-username" class="label-material">User Name</label>
                      </div>
                      <div class="form-group">
                        <input id="login-password" type="password" name="password" required data-msg="Please enter your password" class="input-material">
                        <label for="login-password" class="label-material">Password</label>
                      </div><button type="submit" id="login" href="index.html" class="btn btn-primary">Login</button>
                      <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                    </form><a href="/password/reset" class="forgot-pass">Forgot Password?</a><br>
                      {{--<small>Do not have an account? </small><a href="register.html" class="signup">Signup</a>--}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="copyrights text-center">
          {{--<p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a>--}}
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </p>
        </div>

@endsection

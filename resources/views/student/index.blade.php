@extends('layouts.bootswatch')

@section('content')
    <div class="card-header">Students</div>
    <div class="card-body">
        <div class="mb-3">
            <div class="col-md-9 form-inline" style="float: right;">
                <input type="text" id="searchField" class="form-control mr-2 col-md-9" placeholder="Search ID or Last Name" value="{{ $data['search'] or '' }}"/>
                <button id="search" class="btn btn-primary">Search</button>
            </div>
            <a href="/admin/students/create" class="btn btn-link">Add Student</a>
        </div>
        <table class="table">
            <thead class="table-primary">
            <td style="width: 20%">Student ID</td>
            <td style="width: 25%">Email</td>
            <td style="width: 30%">Name</td>
            <td style="width: 10%">Gender</td>
            <td style="width: 15%">Operation</td>
            </thead>
            @foreach($students as $student)
                <tr>
                    <td>{{$student->id}}</td>
                    <td>{{$student->user->email}}</td>
                    <td>{{$student->first_name}}{{' '.$student->middle_name}}{{' '.$student->family_name}}</td>
                    <td>{{$student->gender}}</td>
                    <td>
                        <a class="btn btn-outline-primary" href="{{ url('/admin/students/' . $student->id) }}"><i class="fa fa-edit"></i> View</a>
                        <a class="btn btn-outline-danger btn" onclick="event.preventDefault();
                            document.getElementById('delete').submit();"><i class="fa fa-edit"></i> Delete</a>
                        <form id="delete" action="{{ url('/admin/students/' . $student->id) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE" >
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $students->appends($data)->links() }}
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var search = function(){
                var searchField = $('#searchField').val();
                if(searchField != ''){
                    location.href = '/admin/students?search='+searchField;
                } else {
                    location.href = '/admin/students';
                }
            };

            $('#searchField').on('keypress', function(e){
                if(e.which == 13) {
                    search();
                }
            });

            $('#search').on('click', function(){
                search();
            });
        });
    </script>
@endsection
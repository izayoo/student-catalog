<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
	protected $guarded = [];

	public $incrementing = false;

	public function loanDetails(){
		return $this->hasOne('App\LoanDetail');
	}

	public function adnuEnrolmentDetail(){
		return $this->hasOne('App\AdnuEnrolmentDetail');
	}

	public function affiliation(){
		return $this->hasMany('App\Affiliation');
	}

	public function disability(){
		return $this->hasMany('App\Disability');
	}

	public function employment(){
		return $this->hasOne('App\Employment');
	}

	public function publication(){
		return $this->hasMany('App\Publication');
	}

	public function scholarshipDetail(){
		return $this->hasMany('App\ScholarshipDetail');
	}

	public function thesis(){
		return $this->hasMany('App\Thesis');
	}

	public function schoolAttended(){
		return $this->hasMany('App\SchoolAttended');
	}

	public function specialAward(){
		return $this->hasMany('App\SpecialAward');
	}

	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function scopeWithRelatedTables(){
		return $this->with(
			'loanDetails',
			'adnuEnrolmentDetail',
			'affiliation',
			'disability',
			'employment',
			'publication',
			'scholarshipDetail',
			'thesis',
			'schoolAttended',
			'specialAward'
		);
	}

	public function deleteRelatedData(){
		$this->loanDetails()->delete();
		$this->adnuEnrolmentDetail()->delete();
		$this->affiliation()->delete();
		$this->disability()->delete();
		$this->employment()->delete();
		$this->publication()->delete();
		$this->scholarshipDetail()->delete();
		$this->thesis()->delete();
		$this->schoolAttended()->delete();
		$this->specialAward()->delete();
	}

	public function delete(){
		$this->loanDetails()->delete();
		$this->adnuEnrolmentDetail()->delete();
		$this->affiliation()->delete();
		$this->disability()->delete();
		$this->employment()->delete();
		$this->publication()->delete();
		$this->scholarshipDetail()->delete();
		$this->thesis()->delete();
		$this->schoolAttended()->delete();
		$this->specialAward()->delete();
		parent::delete();
	}
}

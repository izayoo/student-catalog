<?php

namespace App\Http\Controllers;

use App\Login;
use Illuminate\Http\Request;

class LogsController extends Controller
{
	/**
	 * @var Login
	 */
	private $login;

	/**
	 * @param Login $login
	 */
	public function __construct(Login $login){

		$this->login = $login;
	}

	/**
	 *
	 */
	public function index(){
		$users = $this->login
			->join('users', 'users.user_id', 'logins.user_id')
			->join('user_roles', 'users.user_id', 'user_roles.user_id')
			->join('roles', 'user_roles.role_id', 'roles.role_id')
			->select('*', 'logins.created_at as login_at')
			->orderBy('logins.created_at', 'desc')->paginate(20);
//		dd($users);
//
		return view('logs.index', compact('users'));
	}
}

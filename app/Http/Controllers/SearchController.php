<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Search\SearchRepository;
class SearchController extends Controller
{
    protected $search;

    public function __construct(SearchRepository $search)
    {
        $this->search = $search;
    }

    public function search(){

        $results = $this->search->table('students',function($q){
            //kapag gusto mo kunin lahat ng columns sa parent table
            $q->select('*');

            //kapag specific column lang
            //$q->select('column_name');
            //kapag gusto mo i rename yung column
            //$q->select('column_name',['as' => 'rename_column' ]);

            //kung gusto mo ijoin yung related table nung parent table
            $q->join('school_attendeds' , function($q){
                //dapat lagi nakaset yung foreignkey
               $q->foreignKey('student_id');
               //column method kung anung column lang ang gusto mong kunin
               $q->column('institution');
            });

            //jkung yung jinoin mo na table ay may relation pa ulit
            // $q->joinSub('sample','sample_id');
        })->execute(10);
        dd($results);
    }

}

<?php

namespace App\Http\Controllers;

use App\Coordinator;
use App\Role;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;

class UserController extends Controller
{
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var Role
	 */
	private $role;
	/**
	 * @var UserRole
	 */
	private $userRole;
	/**
	 * @var Coordinator
	 */
	private $coordinator;

	/**
	 * @param User $user
	 * @param Role $role
	 * @param UserRole $userRole
	 * @param Coordinator $coordinator
	 */
	public function __construct(User $user, Role $role, UserRole $userRole, Coordinator $coordinator){
		$this->user = $user;
		$this->role = $role;
		$this->userRole = $userRole;
		$this->coordinator = $coordinator;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	    $data = $request->all();

	    $users = $this->user->join('user_roles', 'users.user_id', 'user_roles.user_id')
		    ->join('roles', 'user_roles.role_id', 'roles.role_id')->orderBy('users.created_at', 'desc');

	    if(isset($request->search))
		    $users = $users->where('email', 'like', $data['search'].'%')
			    ->orWhere('user_name', 'like', $data['search'].'%');

		$users = $users->paginate(10);

	    return view('user.index', compact('users', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $roles = $this->role->all();

	    return view('user.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

	    if(isset($data['redir'])){
		    $redirectTo = implode('/', explode('-', $data['redir']));
		    unset($data['redir']);
	    }

	    $role = $data['role'];
	    unset($data['role']);
	    $unique_id = $this->user->orderBy('user_id', 'desc')->first()->user_id +1;
		$data['user_id'] = $unique_id;
	    $data['password'] = bcrypt('password');

	    $this->user->create($data);

	    $this->userRole->create([
		    'u_role_id' => $this->userRole->orderBy('u_role_id', 'desc')->first()->u_role_id +1,
		    'role_id' => $role,
		    'user_id' => $unique_id
	    ]);

	    if($role == 3)
		    $this->coordinator->create([
			    'coordinator_id' => $this->coordinator->all()->count() +1,
			    'user_id' => $unique_id
		    ]);

	    if(isset($redirectTo)) return redirect($redirectTo);
	    return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $roles = $this->role->all();
	    $model = $this->user->with('role')->whereUserId($id)->first();
	    $mode = 'view';

	    return view('user.add', compact('model','mode','roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $roles = $this->role->all();
	    $model = $this->user->with('role')->whereUserId($id)->first();

	    return view('user.add', compact('model','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $data = $request->all();

	    $role = $data['role'];
	    unset($data['role']);

	    $user = $this->user->whereUserId($id)->first();
	    $user->fill($data)->save();
	    $user->deleteRelatedData();

	    $this->userRole->create([
		    'u_role_id' => $this->userRole->orderBy('created_at', 'desc')->first()->user_id +1,
		    'role_id' => $role,
		    'user_id' => $user->user_id
	    ]);

	    return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->find($id)->delete();

	    return redirect()->back();
    }
}

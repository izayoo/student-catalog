<?php

namespace App\Http\Controllers;

use App\Student;;
use App\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{
	/**
	 * @var Student
	 */
	private $student;
	/**
	 * @var User
	 */
	private $user;

	/**
	 * @param Student $student
	 * @param User $user
	 */
	public function __construct(Student $student, User $user){
		$this->student = $student;
		$this->user = $user;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	    $data = $request->all();

        $students = $this->student->with('user');

	    if(isset($request->search))
		    $students = $students->where('id', 'like', $data['search'].'%')
			    ->orWhere('family_name', 'like', $data['search'].'%')
			    ->orWhere('family_name', 'like', '% '.$data['search'].'%');

        $students = $students->orderBy('created_at', 'desc')->paginate(10);

	    return view('student.index', compact('students', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $students = $this->student->select('user_id')->get()->toArray();
	    $users = $this->user->with('role')
		    ->whereHas('role', function($query){
			    $query->whereRoleId(2);
		    })->whereNotIn('user_id', $students)->get();

	    if($users->count() == 0) return redirect('/admin/users/create')->with('empty', 'There are no users with a <em>student</em> role to create a student record. Please create a new one before proceeding.');

        return view('student.add', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

	    $enrolmentDetails = [
		    'enrolment_type' => $data['enrolment_type'],
		    'previous_student_number' => $data['previous_student_number'],
		    'program' => $data['program']
	    ];

	    $loanDetails = [
		    'lender' => $data['lender']
	    ];

	    $scholarshipDetails = [
		    'type' => $data['type'],
		    'sponsor' => $data['sponsor'],
	    ];

	    $employment = [
		    'position' => $data['position'],
		    'employment_name' => $data['employment_name'],
		    'employment_address' => $data['employment_address'],
	    ];

	    $schoolAttended = [];
	    if(isset($data['schoolAttended']))
		    foreach($data['schoolAttended'] as $key => $val){
			    foreach($val as $key1 => $val1){
				    $schoolAttended[$key1][$key] = $val1;
			    }
		    }
	    $disability = [];
	    if(isset($data['disability']))
		    foreach($data['disability'] as $key => $val){
			    foreach($val as $key1 => $val1){
				    $disability[$key1][$key] = $val1;
			    }
		    }
	    $specialAward = [];
	    if(isset($data['specialAward']))
		    foreach($data['specialAward'] as $key => $val){
			    foreach($val as $key1 => $val1){
				    $specialAward[$key1][$key] = $val1;
			    }
		    }
	    $publication = [];
	    if(isset($data['publication']))
		    foreach($data['publication'] as $key => $val){
			    foreach($val as $key1 => $val1){
				    $publication[$key1][$key] = $val1;
			    }
		    }
	    $affiliation = [];
	    if(isset($data['affiliation']))
		    foreach($data['affiliation'] as $key => $val){
			    foreach($val as $key1 => $val1){
				    $affiliation[$key1][$key] = $val1;
			    }
		    }
	    $thesis = [];
	    if(isset($data['thesis']))
		    foreach($data['thesis'] as $key => $val){
			    foreach($val as $key1 => $val1){
				    $thesis[$key1][$key] = $val1;
			    }
		    }

	    unset($data['enrolment_type']);
	    unset($data['previous_student_number']);
	    unset($data['program']);
	    unset($data['lender']);
	    unset($data['type']);
	    unset($data['sponsor']);
	    unset($data['position']);
	    unset($data['employment_name']);
	    unset($data['employment_address']);
	    unset($data['schoolAttended']);
	    unset($data['disability']);
	    unset($data['specialAward']);
	    unset($data['publication']);
	    unset($data['affiliation']);
	    unset($data['thesis']);

	    $student = $this->student->create($data);
	    $student->deleteRelatedData();

	    if($data['ADNU_enrollee_before'] == 'Yes'){
		    $student->adnuEnrolmentDetail()->create($enrolmentDetails);
	    }

	    if(strtolower($data['financial_support']) == 'loan') {
		    $student->loanDetails()->create($loanDetails);
	    } else if(strtolower($data['financial_support']) == 'scholarship') {
		    $student->scholarshipDetail()->create($scholarshipDetails);
	    }

	    if($employment['position'] != null && $employment['employment_name'] != null && $employment['employment_address'] != null)
		    $student->employment()->create($employment);

	    foreach($schoolAttended as $key => $val){
		    if($key != 0) $student->schoolAttended()->create($val);
	    }
	    foreach($disability as $key => $val){
		    if($key != 0) $student->disability()->create($val);
	    }
	    foreach($specialAward as $key => $val){
		    if($key != 0) $student->specialAward()->create($val);
	    }
	    foreach($affiliation as $key => $val){
		    if($key != 0) $student->affiliation()->create($val);
	    }
	    foreach($publication as $key => $val){
		    if($key != 0) $student->publication()->create($val);
	    }
	    foreach($thesis as $key => $val){
		    if($key != 0) $student->thesis()->create($val);
	    }

	    return redirect('/admin/students');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $model = $this->student->withRelatedTables()->find($id);
		$mode = 'view';

	    return view('student.add', compact('model', 'mode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $model = $this->student->withRelatedTables()->find($id);

	    return view('student.add', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $data = $request->all();

	    $enrolmentDetails = [
		    'enrolment_type' => $data['enrolment_type'],
		    'previous_student_number' => $data['previous_student_number'],
		    'program' => $data['program']
	    ];

	    $loanDetails = [
		    'lender' => $data['lender']
	    ];

	    $scholarshipDetails = [
		    'type' => $data['type'],
		    'sponsor' => $data['sponsor'],
	    ];

	    $employment = [
		    'position' => $data['position'],
		    'employment_name' => $data['employment_name'],
		    'employment_address' => $data['employment_address'],
	    ];

	    $schoolAttended = [];
	    if(isset($data['schoolAttended']))
	    foreach($data['schoolAttended'] as $key => $val){
		    foreach($val as $key1 => $val1){
			    $schoolAttended[$key1][$key] = $val1;
		    }
	    }
	    $disability = [];
	    if(isset($data['disability']))
	    foreach($data['disability'] as $key => $val){
		    foreach($val as $key1 => $val1){
			    $disability[$key1][$key] = $val1;
		    }
	    }
	    $specialAward = [];
	    if(isset($data['specialAward']))
	    foreach($data['specialAward'] as $key => $val){
		    foreach($val as $key1 => $val1){
			    $specialAward[$key1][$key] = $val1;
		    }
	    }
	    $publication = [];
	    if(isset($data['publication']))
	    foreach($data['publication'] as $key => $val){
		    foreach($val as $key1 => $val1){
			    $publication[$key1][$key] = $val1;
		    }
	    }
	    $affiliation = [];
	    if(isset($data['affiliation']))
	    foreach($data['affiliation'] as $key => $val){
		    foreach($val as $key1 => $val1){
			    $affiliation[$key1][$key] = $val1;
		    }
	    }
	    $thesis = [];
	    if(isset($data['thesis']))
	    foreach($data['thesis'] as $key => $val){
		    foreach($val as $key1 => $val1){
			    $thesis[$key1][$key] = $val1;
		    }
	    }

	    unset($data['enrolment_type']);
	    unset($data['previous_student_number']);
	    unset($data['program']);
	    unset($data['lender']);
	    unset($data['type']);
	    unset($data['sponsor']);
	    unset($data['position']);
	    unset($data['employment_name']);
	    unset($data['employment_address']);
	    unset($data['schoolAttended']);
	    unset($data['disability']);
	    unset($data['specialAward']);
	    unset($data['publication']);
	    unset($data['affiliation']);
	    unset($data['thesis']);

	    $student = $this->student->withRelatedTables()->find($id);
	    $student->deleteRelatedData();
	    $student->fill($data)->save();

	    if($data['ADNU_enrollee_before'] == 'Yes'){
		    $student->adnuEnrolmentDetail()->create($enrolmentDetails);
	    }

	    if(strtolower($data['financial_support']) == 'loan') {
		    $student->loanDetails()->create($loanDetails);
	    } else if(strtolower($data['financial_support']) == 'scholarship') {
		    $student->scholarshipDetail()->create($scholarshipDetails);
	    }

	    if($employment['position'] != null && $employment['employment_name'] != null && $employment['employment_address'] != null)
		    $student->employment()->create($employment);

	    foreach($schoolAttended as $key => $val){
		    if($key != 0) $student->schoolAttended()->create($val);
	    }
	    foreach($disability as $key => $val){
		    if($key != 0) $student->disability()->create($val);
	    }
	    foreach($specialAward as $key => $val){
		    if($key != 0) $student->specialAward()->create($val);
	    }
	    foreach($affiliation as $key => $val){
		    if($key != 0) $student->affiliation()->create($val);
	    }
	    foreach($publication as $key => $val){
		    if($key != 0) $student->publication()->create($val);
	    }
	    foreach($thesis as $key => $val){
		    if($key != 0) $student->thesis()->create($val);
	    }

	    return redirect('/admin/students/'.$student->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $this->student->find($id)->delete();

	    return redirect()->back();
    }
}

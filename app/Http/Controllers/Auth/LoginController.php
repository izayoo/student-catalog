<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Login;
use App\Student;
use App\User;
use App\UserRole;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';
	/**
	 * @var Login
	 */
	private $login;
	/**
	 * @var UserRole
	 */
	private $userRole;
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var Student
	 */
	private $student;

	/**
	 * Create a new controller instance.
	 *
	 * @param Login $login
	 * @param UserRole $userRole
	 * @param User $user
	 * @param Student $student
	 */
    public function __construct(Login $login, UserRole $userRole, User $user, Student $student)
    {
        $this->middleware('guest')->except('logout');
	    $this->login = $login;
	    $this->userRole = $userRole;
	    $this->user = $user;
	    $this->student = $student;
    }

	/**
	 * The user has been authenticated.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  mixed  $user
	 * @return mixed
	 */
	protected function authenticated(Request $request, $user)
	{
		$this->login->create(['user_id' => $user->user_id]);

		$userRole = $this->userRole->with('role')->whereUserId($user->user_id)->first();
		if($userRole->role->role_name == 'admin' || $userRole->role->role_name == 'coordinator') return redirect('admin/users');
		else return redirect('profile');
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function logout(Request $request)
	{
		$this->guard()->logout();

		$request->session()->invalidate();

		return $this->loggedOut($request) ?: redirect('/');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
	 *
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function login(Request $request)
	{
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if ($this->hasTooManyLoginAttempts($request)) {
			$this->fireLockoutEvent($request);

			return $this->sendLockoutResponse($request);
		}

		$user = $this->user->join('user_roles', 'users.user_id', 'user_roles.user_id')
			->join('roles', 'user_roles.role_id', 'roles.role_id')
			->where('email', $request->email)->first();

		if($user->role_id == 2) {
			$login = $this->student->where('user_id', $user->user_id)->get()->count() == 0 ? false : true;
		} else {
			$login = true;
		}

		if ($login && $this->attemptLogin($request)) {
			return $this->sendLoginResponse($request);
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}
}

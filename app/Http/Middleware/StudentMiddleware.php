<?php

namespace App\Http\Middleware;

use App\Student;
use App\UserRole;
use Closure;
use Illuminate\Http\Response;

class StudentMiddleware
{
	/**
	 * @var UserRole
	 */
	private $userRole;
	/**
	 * @var Student
	 */
	private $student;

	/**
	 * @param UserRole $userRole
	 * @param Student $student
	 */
	public function __construct(UserRole $userRole, Student $student){

		$this->userRole = $userRole;
		$this->student = $student;
	}

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if ($request->user()){
			$user = $request->user();
		    $userRole = $this->userRole->with('role')->whereUserId($user->user_id)->first();
		    if($userRole->role->role_name != 'student')
			    return new Response(view('unauthorized')->with('role', 'student'));
	    }

	    return $next($request);
    }
}

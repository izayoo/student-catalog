<?php

namespace App\Http\Middleware;

use App\UserRole;
use Closure;
use Illuminate\Http\Response;

class AdminMiddleware
{
	/**
	 * @var UserRole
	 */
	private $userRole;

	/**
	 * @param UserRole $userRole
	 */
	public function __construct(UserRole $userRole){

		$this->userRole = $userRole;
	}

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	public function handle($request, Closure $next)
	{

		if ($request->user()){
			$user = $request->user();
			$userRole = $this->userRole->with('role')->whereUserId($user->user_id)->first();
			if($userRole->role->role_name != 'admin' && $userRole->role->role_name != 'coordinator')
				return new Response(view('unauthorized')->with('role', 'admin'));
		}


		return $next($request);
	}
}

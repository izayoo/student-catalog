<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disability extends Model
{
	protected $primaryKey = 'disability_entry_id';

	protected $guarded = [];
}

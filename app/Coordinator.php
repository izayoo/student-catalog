<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
	protected $primaryKey = 'coordinator_id';

	protected $guarded = [];
}

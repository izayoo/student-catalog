<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
	protected $primaryKey = 'employment_entry_id';

	protected $guarded = [];
}

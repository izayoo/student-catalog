<?php
namespace App\Repositories\Search;
use DB;
class SearchRepository
{
    /**
     * Collection
     * @var
     */
    protected $collection;

    /**
     * Filter query
     * type array( key must be column name)
     * @var $by
     */
    protected $filter_by = [];
    
    /**
     * Keyword to query
     * type string(any)
     * @var $key_word
     */
    protected $key_word = null;

    /**
     * Tables to join in parent table
     * type array( tables )
     * @var $joined_tables
     */
    protected $joined_tables = [];

    /**
     * Parent table
     * type string(table)
     * @var $parent_table
     */
    protected $parent_table;
    
    /**
     * Foreign key of each table joined.
     * @var $foreign_key
     */
    protected $foreign_key;

    /**
     * Columns to select of joined tables
     * @var $columns
     */
    protected $columns = [];

    /**
     * Columns to select of parent table
     * @var $parent_table_columns
     */
    protected $parent_table_columns = [];

    /**
     * tables that you want to query from join table
     * @var $join_sub
     */
    protected $join_sub = [];

    /**
     * Set filter query
     * @param string $column_name
     * @param $value
     * @return $this
     */
    public function filterBy($column_name,$value){

        $this->filter_by[$column_name] = $value;
        
        return $this;
    }

    /**
     * Set key word to query
     * @param string $key_word
     * @return $this
     */
    public function keyWord($key_word){
        $this->key_word = $key_word;
        return $this;
    }

    /**
     * Set parent table
     * @param string table name
     * @param function $function
     * @return $this
     */
    public function table($parent_table,$function){
        $function($this);
        $this->parent_table = $parent_table;
        return $this;
    }

    /**
     * Select specific columns of the parent table
     * @param string $column_name
     * @param string $set_as
     * @return $this
     */
    public function select($column_name,$set_as = null){
        if($column_name == "*"){
            $this->parent_table_columns = '*';
            return $this;
        }
        if($set_as != null){
            $this->parent_table_columns[$column_name] = $set_as;
        }else{
            $this->parent_table_columns[] = $column_name;
        }
        return $this;
    }
    /**
     * Set tables to join in parent table
     * @param $tables ( can be array or string)
     * @return $this
     */
    public function join($table,$function){

        $function($this);

        $this->joined_tables[$table]['foreign_key'] = $this->foreign_key;
        $this->joined_tables[$table]['columns'] = $this->columns;
        $this->columns = [];
        return $this;
            
    }

    /**
     * Set the foreign key of each joined table
     * @param string $value (column name)
     * @return $this
     */
    public function foreignKey($value){
        $this->foreign_key = $value;
        return $this;
    }

    /**
     * Set the columns to select in join table
     * @param string $value (column name)
     */
    public function column($value){

        if(getType($value) == 'array'){
            foreach($value as $v){
                $this->columns[] = $v;
            }
        }else{
            $this->columns[] = $value;

        }
        return $this;
    }

    /**
     * If join table has a relation and you want to query that relation table.
     * @param string $table 
     * @param string $foreignKey
     */
    public function joinSub($table,$foreignKey){

        $this->join_sub[$table] = $foreignKey;
        
        return $this;
    }

    /**
     * Set collections parent table columns
     * @return void
     */
    protected function setCollectionParentTableColumns(){
        if($this->parent_table_columns == "*"){
            $this->collection->select($this->parent_table.'.'.$this->parent_table_columns);
        }else{
            foreach($this->parent_table_columns as $ptck =>  $ptc){
                if(getType($ptc) == 'array'){
                    if(isset($ptc['as'])){
                        foreach($ptc as $k => $v){
                            $this->collection->addSelect($this->parent_table.'.'.$ptck.' '.$k.' '.$v);
                        }
                    }
                }else{
                    $this->collection->addSelect($this->parent_table.'.'.$ptc);
                }    
            }
        }

    }

    /**
     * Join related tables to the collection
     * @return void
     */
    protected function joinTables(){
        foreach($this->joined_tables as $jtk => $jt){
            $this->collection->join($jtk,$this->parent_table.'.id','=',$jtk.'.'.$jt['foreign_key']);
            foreach($jt['columns'] as $c){
                $this->collection->addSelect($jtk.'.'.$c);
            }
        }
    }

    /**
     * Join sub related tables to the collection
     * @return void
     */
    protected function joinSubtables(){
        $current_collection = $this->collection;
        $next_collection = null;
        if(count($this->join_sub) > 0){
            foreach($this->join_sub as $table_name =>  $table_foreign_key){
                $next_collection = DB::table($table_name)
                ->joinSub($current_collection,$this->parent_table,function($join) use ($table_name,$table_foreign_key){
                    $join->on($table_name.'.id','=',$this->parent_table.'.'.$table_foreign_key);
                });
                
                $current_collection = $next_collection;
            }
        }

        $this->collection = $current_collection;
    }

    /**
     * Set collection filters
     * @return void
     */
    protected function setCollectionFilters(){
        if(count($this->filter_by) > 0){
            foreach($this->filter_by as $fbk => $fbv){ 
                if(getType($fbv) == 'array'){
                    $this->collection->where($fbk,'>=',min($fbv));
                    $this->collection->where($fbk,'<=',max($fbv));
                }else{
                    $this->collection->where($fbk,$fbv);
                }
                
            }
        }
    }

    /**
     * Set columns to search including join tables and join sub table( columns can be set to config/search.php)
     * @return void
     */
    protected function setCollectionColumnsToSearch(){

        foreach(\Config::get('search') as $k => $c){
            if($k == 0){
                $this->collection   =   $this->collection->where($c, 'like', '%' . $this->key_word . '%');
            }else{
                $this->collection   =   $this->collection->orWhere($c, 'like', '%' . $this->key_word . '%');
            }
        }
    }


    /**
     * Execute the query
     * @param integer $limit
     * @return object search results
     */
    public function execute($limit){
        $this->collection = DB::table($this->parent_table);
        if($this->parent_table_columns != "*"){
            $this->collection->select($this->parent_table.'.id');            
        }
        
        $this->setCollectionParentTableColumns();

        if(count($this->joined_tables) > 0){
            $this->joinTables();
        }


        if(count($this->join_sub) > 0){
            $this->joinSubtables();
        }

        if(count($this->filter_by) > 0){
            $this->setCollectionFilters();
        }
        

        if($this->key_word != null){
            $this->setCollectionColumnsToSearch();
        }
        

       $this->collection->groupBy($this->parent_table.'.id');

        return $this->collection->limit($limit)->get();
    }

}
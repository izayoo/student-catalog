<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolAttended extends Model
{

	protected $primaryKey = 'school_attended_entry_id';
	
	protected $guarded = [];
}

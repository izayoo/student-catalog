<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{

    //
	protected $guarded = [];
    protected $primaryKey = 'u_role_id';
    
    public function role(){
        return $this->hasOne(Role::class,'role_id','role_id')->with('permission');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    //

    public function permission(){
        return $this->hasOne(Permission::class,'permission_id','permission_id');
    }
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'email', 'password','user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function role(){
        return $this->hasOne('App\UserRole','user_id')->with('role');
    }

    public function student(){
        return $this->hasOne('App\Student','user_id');
    }

    public function login(){
        return $this->hasMany('App\Login','user_id');
    }

	public function deleteRelatedData(){
		$this->role()->delete();
		$this->student()->delete();
		$this->login()->delete();
	}

	public function delete(){
		$this->role()->delete();
		$this->student()->delete();
		$this->login()->delete();
		parent::delete();
	}
}

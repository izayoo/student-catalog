<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialAward extends Model
{
	protected $primaryKey = 'special_award_entry_id';
	
	protected $guarded = [];
}

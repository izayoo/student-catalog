<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
	protected $primaryKey = 'log_in_id';

	protected $guarded = [];

	public function user(){
		return $this->belongsTo('App\User', 'user_id')->with('role');
	}
}

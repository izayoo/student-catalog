<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $primaryKey = 'role_id';
    
    public function permission(){
        return $this->hasMany(RolePermission::class,'role_id','role_id')->with('permission');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thesis extends Model
{
	protected $primaryKey = 'thesis_entry_id';
	
	protected $guarded = [];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
	protected $primaryKey = 'publication_id_entry';
	protected $guarded = [];
}

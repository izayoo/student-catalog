<?php

namespace App\Customfunctions;
use Auth;
/**
 * YOU CAN STORE YOUR CUSTOM FUNCTIONS HERE INCASE YOU NEED IT IN YOUR FUTURE CODES.
 */
trait func
{
    /**
     * GENERATE UNIQUE ID
     * @param $length
     * @return int $id
     */
    public function generateID(int $unique){
        $str_len = strlen($unique);
        $length = $str_len > 1 ? 9 - (int)$str_len : 10 - (int)$str_len;
        $id = '1';
        for ($i=0; $i < $length; $i++) { 
            $id .= '0';
        }
        $id .= (string)$unique;
        return (int)$id;
    }


    /**
     * CHECK USER if has PERMISSION
     * @param string user action
     * @return boolean
     */
    public function userHasPermission($action){
        $permissions = Auth::user()->role->role->permission;

        foreach($permissions as $p){
            if($p->permission->permission_name == $action){
                return true;
            }
        }

        return false;
    }

    /**
     * Return users permissions
     * 
     * @return array
     */
    public function userPermissions(){
        $permissions = Auth::user()->role->role->permission;
        $array = [];
        foreach($permissions as $p){
            $array [] = $p->permission->permission_name;
        }
        return $array;
    }
}
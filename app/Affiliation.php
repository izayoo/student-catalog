<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliation extends Model
{
	protected $primaryKey = 'affiliation_entry_id';

	protected $guarded = [];
}
